using ConsoleAppDotNetCore;
using Microsoft.VisualStudio.TestPlatform.TestHost;
using NUnit.Framework;

namespace NUnitTestProject
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestStandard()
        {
            Assert.Pass();
        }

        [Test]
        public void TestCheckWin_ReturnsFalse()
        {
            var clStandard = new ClassStandard();

            var result = clStandard.CheckCondition();

            Assert.IsFalse(result);
        }

    }
}